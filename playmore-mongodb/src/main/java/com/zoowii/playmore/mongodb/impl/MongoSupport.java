package com.zoowii.playmore.mongodb.impl;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.zoowii.jpa_utils.util.StringUtil;
import com.zoowii.playmore.mongodb.IMongoSupport;
import org.bson.Document;
import zuice.annotations.Component;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zoowii on 15/1/23.
 */
@Component
public class MongoSupport implements IMongoSupport {
    private MongoClient mongoClient;
    private String dbName;
    private String username;
    private String password;

    /**
     * @param mongoUri eg. mongodb://localhost:27017,localhost:27018,localhost:27019
     * @param dbName   the default database name
     */
    public MongoSupport(String mongoUri, String dbName) {
        this.dbName = dbName;
        mongoClient = new MongoClient(new MongoClientURI(mongoUri));
    }

    @Override
    public MongoClient getMongoClient() {
        return mongoClient;
    }

    @Override
    public MongoDatabase getDB(String dbName) {
        return mongoClient.getDatabase(dbName);
    }

    @Override
    public MongoDatabase getDefaultDB() {
        return mongoClient.getDatabase(dbName);
    }

    @Override
    public void setDefaultDbName(String dbName) {
        this.dbName = dbName;
    }

    @Override
    public MongoCollection<Document> getCollection(String dbName, String collectionName) {
        return getDB(dbName).getCollection(collectionName);
    }

    @Override
    public <T> MongoCollection<T> getCollection(String dbName, String collectionName, Class<T> documentCls) {
        return getDB(dbName).getCollection(collectionName, documentCls);
    }

    @Override
    public MongoCollection<Document> getCollection(String collectionName) {
        return getCollection(dbName, collectionName);
    }

    @Override
    public <T> MongoCollection<T> getCollection(String collectionName, Class<T> documentCls) {
        return getCollection(dbName, collectionName, documentCls);
    }

    @Override
    public void setOptions(int options) {
        mongoClient.setOptions(options);
    }
}
