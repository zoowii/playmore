package com.zoowii.playmore.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Created by zoowii on 15/1/23.
 */
public interface IMongoSupport {
    public MongoClient getMongoClient();

    public MongoDatabase getDB(String dbName);

    public MongoDatabase getDefaultDB();

    public void setDefaultDbName(String dbName);

    public MongoCollection<Document> getCollection(String dbName, String collectionName);

    public <T> MongoCollection<T> getCollection(String dbName, String collectionName, Class<T> documentCls);

    public MongoCollection<Document> getCollection(String collectionName);

    public <T> MongoCollection<T> getCollection(String collectionName, Class<T> documentCls);

    public void setOptions(int options);
}
