package com.zoowii.playmore.runner;

/**
 * Created by zoowii on 15/1/24.
 */
public interface IServer {
    public void start();

    public void stop();
}