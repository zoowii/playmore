playmore
====
A simple Web Framework of Java

## Usage

* deploy to maven repository or install to maven locally using
    mvn install:install-file -DgroupId=com.zoowii -DartifactId=playmore -Dversion=x.y.z -Dpackaging=jar -Dfile=/path/to/playmore-x.y.z.jar

* add a playmore.xml to resources directory or classpath, and modify it like the demo

* do anything you like!

## Demo

* demo project is the 'playmore-example' sub-module under the playmore-parent project


## Dependencies

* Java 7
* Maven
* 部署https://bitbucket.org/zoowii/Zuice, https://github.com/zoowii/JPAUtils, https://bitbucket.org/zoowii/jany-route, https://github.com/zoowii/form-utils 到本地maven私服或者其他可以获取到这些库的maven仓库,或者下载并运行install_dependencies.sh来安装lib文件夹下的jar包到本地maven


## Features

* 类似Spring的依赖注入(使用zoowii/zuice)
* 类似Spring MVC的MVC框架
* 内置类ActiveRecord风格的orm库(使用zoowii/JPAUtils),同时支持类似Spring Transaction的事务管理
* 支持单元测试(类似Spring可以使用配置文件)
* form validation支持(使用zoowii/form-utils)
* 友好的路由机制(同时支持类似Spring MVC的Controller的方式,和独立的Action类的方式,路由的语法和参数解析使用也非常方便,类似于Python的Flask框架)(使用zoowii/jany-route)
* 支持jetty-runner独立运行模式,并且在IDEA下开发时,使用debug运行可以在compile文件后自动reload(IDEA菜单中的Build->Compile ...)
* 方便自定义Session的实现(无论放入内存还是memcached/redis)
* 作者还活着,有问题可以直接打扰