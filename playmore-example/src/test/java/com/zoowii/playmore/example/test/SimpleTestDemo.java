package com.zoowii.playmore.example.test;

import com.zoowii.playmore.demo.models.TestUserEntity;
import com.zoowii.playmore.demo.services.ITestUserService;
import com.zoowii.playmore.test.PlayMoreTestCase;
import com.zoowii.playmore.test.RunWith;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by zoowii on 15/1/23.
 */
// you can set another config xml file only for test, and use <import .../> to use the same config
@RunWith("playmore.xml")
public class SimpleTestDemo extends PlayMoreTestCase {
    @Test
    public void testUserService() {
        ITestUserService testUserService = injector.getBean(ITestUserService.class);
        int n = 10;
        for (int i = 0; i < n; ++i) {
            TestUserEntity testUserEntity = new TestUserEntity();
            testUserEntity.setAge(new Random().nextInt());
            testUserEntity.setName("test_user_" + UUID.randomUUID().toString());
            testUserService.addUser(testUserEntity);
        }
        List<TestUserEntity> testUserEntities = testUserService.listUsers();
        assertEquals(testUserEntities.size(), n);
    }
}
