package com.zoowii.playmore.demo.services.impl;

import com.zoowii.jpa_utils.core.SessionFactory;
import com.zoowii.playmore.annotation.Service;
import com.zoowii.playmore.demo.models.TestUserEntity;
import com.zoowii.playmore.demo.services.ITestUserService;
import zuice.annotations.Autowired;

import java.util.List;

/**
 * Created by zoowii on 2015/1/18.
 */
@Service
public class TestUserService implements ITestUserService {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(TestUserEntity testUserEntity) {
        if (testUserEntity != null) {
            testUserEntity.save(sessionFactory.currentSession());
        }
    }

    @Override
    public List<TestUserEntity> listUsers() {
        return TestUserEntity.find.findAll();
    }
}
