package com.zoowii.playmore.demo;

import com.zoowii.playmore.runner.IServer;
import com.zoowii.playmore.runner.JettyServer;
import com.zoowii.playmore.runner.utils.PathUtil;

/**
 * 直接咨询这个类会自动一个jetty来运行webapp,并且支持在IDEA的debug模式下运行时自动reload程序(如果只是compile一个类,其他类不会受影响,所以一些已经加载的东西不会自动更新,除非rebuild整个项目)
 * 目前这个只在IDEA下运行过,eclipse不一定支持,但是改一下下面代码中的路径应该就可以了
 * Created by zoowii on 15/1/24.
 */
public class Main {
    public static void main(String[] args) {
        PathUtil.setWebRootPath(PathUtil.getWebRootPath() + "/target/playmoredemo");
        IServer server = new JettyServer(PathUtil.getWebRootPath(), 8080, "/", 0);
        server.start();
    }
}
