package com.zoowii.playmore.demo;

import com.zoowii.formutils.BindingResult;
import com.zoowii.formutils.Validator;
import com.zoowii.formutils.annotations.Email;
import com.zoowii.formutils.annotations.Length;
import com.zoowii.formutils.annotations.NotEmpty;
import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.ActionResults;
import com.zoowii.playmore.annotation.Controller;
import com.zoowii.playmore.annotation.Route;
import com.zoowii.playmore.http.FlashMessageSupport;
import com.zoowii.playmore.http.HttpServletRequestWrapper;
import com.zoowii.playmore.security.Authenticated;
import com.zoowii.playmore.demo.models.TestUserEntity;
import com.zoowii.playmore.demo.security.SimpleSecured;
import com.zoowii.playmore.demo.services.ITestUserService;
import com.zoowii.playmore.template.RenderFactory;
import zuice.annotations.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by zoowii on 2015/1/15.
 */
@Controller
@Route("")
public class DemoController {

    @Autowired
    private ITestUserService testUserService;

    @Autowired
    private FlashMessageSupport flashMessageSupport;

    @Autowired
    private Validator validator;

    @Autowired
    private RenderFactory renderFactory;

    @Route({"", "/", "/index"})
    public ActionResult index(HttpServletRequest request, HttpServletResponse response) {
//        return ActionResults.ok("hello, this is index page");
        return ActionResults.ok(renderFactory.create("site/index.vm").renderWith());
    }

    @Route(value = "/user/:id/test/:name", methods = {Route.GET, Route.POST, Route.ANY})
    public ActionResult viewUser(int id, String name) {
        return ActionResults.ok(String.format("user id is %d and name is %s", id, name));
    }

    @Route("/test/jpa")
    public ActionResult addUser() {
        TestUserEntity testUserEntity = new TestUserEntity();
        testUserEntity.setName("test_user_" + UUID.randomUUID().toString());
        testUserEntity.setAge(new Random().nextInt());
        testUserService.addUser(testUserEntity);
        return ActionResults.ok(testUserEntity);
    }

    @Route("/test/users")
    public ActionResult viewUsers() {
        return ActionResults.ok(testUserService.listUsers());
    }

    public static class DemoForm {
        @NotNull
        @Length(min = 4, max = 30)
        private String name;
        @Email
        @NotEmpty(message = "The email can't be empty")
        private String email;
        @Min(18)
        @Max(150)
        private int age;
        @Past
        private Date createTime;
    }

    @Route(value = "/test/form", methods = {Route.POST, Route.GET})
    public ActionResult testForm(HttpServletRequestWrapper request) {
        if (request.getMethod().toLowerCase().equals("get")) {
            return ActionResults.ok(renderFactory.create("formdemo.vm").renderWith());
        }
        DemoForm demoForm = request.asForm(DemoForm.class);
        BindingResult bindingResult = validator.validate(demoForm);
        return ActionResults.ok(String.format("there are %d errors in the form", bindingResult.getErrorCount()));
    }

    @Route(value = "/test/flash")
    public ActionResult testFlash(HttpServletRequestWrapper request) {
        request.flash("alert", "test alert");
        return ActionResults.ok(request.strFlash("alert"));
    }

    @Route("/test/urlfor")
    public ActionResult testUrlFor(HttpServletRequestWrapper request) {
        return ActionResults.ok("test reverse url is " + request.urlFor(DemoController.class, "viewUser", 123, "zoowii"));
    }

    @Authenticated(SimpleSecured.class)
    @Route("/test/security")
    public ActionResult testSecurity(HttpServletRequestWrapper request) {
        return ActionResults.ok("current username is " + request.getAttribute("username"));
    }

    @Route("/test/template")
    public ActionResult testTemplate() {
        return ActionResults.ok(renderFactory.create("test.vm").renderWith("name", "zoowii"));
    }
}
