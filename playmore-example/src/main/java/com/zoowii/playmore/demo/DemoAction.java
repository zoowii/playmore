package com.zoowii.playmore.demo;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.ActionResults;
import com.zoowii.playmore.action.impl.HttpAction;
import com.zoowii.playmore.annotation.Action;
import com.zoowii.playmore.annotation.Route;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zoowii on 2015/1/15.
 */
@Action
@Route("/demo/action")
public class DemoAction extends HttpAction {
    public ActionResult any(HttpServletRequest request, HttpServletResponse response) {
        return ActionResults.ok("hello, action dummy from any method");
    }

    public ActionResult get(HttpServletRequest request, HttpServletResponse response) {
        return ActionResults.ok("hello, action dummy");
    }
}
