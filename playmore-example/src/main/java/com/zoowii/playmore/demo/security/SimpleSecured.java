package com.zoowii.playmore.demo.security;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.ActionResults;
import com.zoowii.playmore.http.HttpServletRequestWrapper;
import com.zoowii.playmore.security.Authenticator;

import java.net.HttpURLConnection;

/**
 * Created by zoowii on 15/1/22.
 */
public class SimpleSecured extends Authenticator {
    @Override
    public String getUsername(HttpServletRequestWrapper request) {
        if (request.getSession().getAttribute("username") != null) {
            String username = request.getSession().getAttribute("username").toString();
            request.setAttribute("username", username);
            return username;
        }
        String username = request.getHttpServletRequest().getParameter("username");
        if (username == null) {
            return null;
        }
        request.getHttpServletRequest().setAttribute("username", username);
        request.getSession().setAttribute("username", username);
        return username;
    }

    @Override
    public ActionResult onUnauthorized(HttpServletRequestWrapper request) {
        return ActionResults.ok("authenticated failed, you can put 'username=anything' in query string").setStatusCode(HttpURLConnection.HTTP_UNAUTHORIZED);
    }
}
