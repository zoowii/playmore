package com.zoowii.playmore.demo.services;

import com.zoowii.playmore.demo.models.TestUserEntity;

import java.util.List;

/**
 * Created by zoowii on 2015/1/18.
 */
public interface ITestUserService {
    void addUser(TestUserEntity testUserEntity);

    List<TestUserEntity> listUsers();
}
