package com.zoowii.playmore.template;

/**
 * Created by zoowii on 15/1/23.
 */
public interface RenderFactory {
    public Render create(String templateName);

    public Render create(String templateName, String encoding);
}
