package com.zoowii.playmore.template.impl;

import com.zoowii.playmore.http.HttpUtils;
import com.zoowii.playmore.route.impl.HttpRouterLoader;
import com.zoowii.playmore.template.AbstractRender;
import com.zoowii.playmore.template.Render;
import com.zoowii.playmore.template.RenderContext;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import java.io.StringWriter;

/**
 * Created by zoowii on 15/1/23.
 */
public class VelocityRender extends AbstractRender {
    private Template template;

    public VelocityRender(Template template) {
        this.template = template;
    }

    @Override
    public String render(RenderContext ctx) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("contextPath", HttpRouterLoader.getWebContextPath());
        velocityContext.put("request", HttpUtils.getCurrentHttpRequest());
        for (String key : ctx.keySet()) {
            velocityContext.put(key, ctx.get(key));
        }
        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        stringWriter.flush();
        return stringWriter.toString();
    }
}
