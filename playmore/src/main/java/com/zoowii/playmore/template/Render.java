package com.zoowii.playmore.template;

/**
 * Created by zoowii on 15/1/23.
 */
public interface Render {
    public String render(RenderContext ctx);

    public String renderWith(Object... bindings);
}
