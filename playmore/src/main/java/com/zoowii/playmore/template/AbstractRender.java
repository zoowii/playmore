package com.zoowii.playmore.template;

/**
 * Created by zoowii on 15/1/23.
 */
public abstract class AbstractRender implements Render {
    @Override
    public String renderWith(Object... bindings) {
        RenderContext ctx = new RenderContext();
        for (int i = 0; i < bindings.length - 1; i += 2) {
            ctx.put(bindings[i].toString(), bindings[i + 1]);
        }
        return render(ctx);
    }
}
