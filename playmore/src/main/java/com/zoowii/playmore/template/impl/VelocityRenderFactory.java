package com.zoowii.playmore.template.impl;

import com.zoowii.jpa_utils.util.StringUtil;
import com.zoowii.playmore.template.Render;
import com.zoowii.playmore.template.RenderFactory;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import zuice.annotations.Component;

import java.io.File;
import java.nio.file.Paths;

/**
 * Created by zoowii on 15/1/23.
 */
@Component
public class VelocityRenderFactory implements RenderFactory {

    /**
     * You can set these properties
     */
    private String propertiesFileName = null;
    private String templateDirPath = null;

    private VelocityEngine velocityEngine;

    /**
     * You must set init-method in beans xml config or manually call init method
     */
    public void init() {
        velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        if (propertiesFileName != null) {
            velocityEngine.init(propertiesFileName);
        } else {
            velocityEngine.init();
        }
    }

    private String getTemplatePath(String name) {
        if (StringUtil.isEmpty(name)) {
            return name;
        }
        return Paths.get(templateDirPath, name).toString();
    }

    @Override
    public Render create(String templateName) {
        return new VelocityRender(velocityEngine.getTemplate(getTemplatePath(templateName), "UTF-8"));
    }

    @Override
    public Render create(String templateName, String encoding) {
        return new VelocityRender(Velocity.getTemplate(getTemplatePath(templateName), encoding));
    }
}
