package com.zoowii.playmore.persistent;

import com.zoowii.playmore.exceptions.TransactionException;

/**
 * Created by zoowii on 2015/1/18.
 */
public interface ITransactionManager {
    public Object doGetTransaction() throws TransactionException;

    public void doBegin() throws TransactionException;

    public void doCommit() throws TransactionException;

    public void doRollback() throws TransactionException;

    public void doClose() throws TransactionException;
}
