package com.zoowii.playmore.persistent;

import com.zoowii.playmore.exceptions.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.annotations.Autowired;
import zuice.aop.JoinPoint;
import zuice.aop.annotations.After;
import zuice.aop.annotations.Aspect;
import zuice.aop.annotations.Before;

/**
 * Created by zoowii on 2015/1/18.
 */
@Aspect
public class JPAUtilsTransactionAdvise {
    private static final Logger LOG = LoggerFactory.getLogger(JPAUtilsTransactionAdvise.class);

    @Autowired
    private ITransactionManager transactionManager;

    @Before("txPoint()")
    public void beforeService(JoinPoint joinPoint) {
        System.out.println("before service");
        LOG.info("before service");
        try {
            transactionManager.doBegin();
        } catch (TransactionException e) {
            throw new RuntimeException(e);
        }
    }

    @After("txPoint()")
    public void afterService(JoinPoint joinPoint) {
        System.out.println("after service");
        LOG.info("after service");
        try {
            transactionManager.doCommit();
        } catch (Exception e) {
            LOG.error("transaction commit error", e);
            try {
                transactionManager.doRollback();
            } catch (TransactionException e1) {
                e1.printStackTrace();
                LOG.error("transaction rollback error", e1);
            }
            throw new RuntimeException(e);
        } finally {
            try {
                transactionManager.doClose();
            } catch (TransactionException e) {
                e.printStackTrace();
                LOG.error("transaction close error", e);
            }
        }
    }
}
