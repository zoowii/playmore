package com.zoowii.playmore.persistent.impl;

import com.zoowii.jpa_utils.core.SessionFactory;
import com.zoowii.playmore.exceptions.TransactionException;
import com.zoowii.playmore.persistent.ITransactionManager;
import zuice.annotations.Autowired;
import zuice.annotations.Component;

/**
 * Created by zoowii on 2015/1/18.
 */
@Component
public class JPAUtilsTransactionManager implements ITransactionManager {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Object doGetTransaction() throws TransactionException {
        return sessionFactory.currentSession();
    }

    @Override
    public void doBegin() throws TransactionException {
        sessionFactory.currentSession().begin();
    }

    @Override
    public void doCommit() throws TransactionException {
        if (!sessionFactory.currentSession().isTransactionActive()) {
            sessionFactory.currentSession().begin();
        }
        sessionFactory.currentSession().commit();
    }

    @Override
    public void doRollback() throws TransactionException {
        sessionFactory.currentSession().rollback();
    }

    @Override
    public void doClose() throws TransactionException {
        sessionFactory.currentSession().close();
    }

}
