package com.zoowii.playmore.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.aop.JoinPoint;
import zuice.aop.annotations.After;
import zuice.aop.annotations.Aspect;
import zuice.aop.annotations.Before;

/**
 * Created by zoowii on 2015/1/15.
 */
@Aspect
public class SimpleLogAdvice {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleLogAdvice.class);

    @SuppressWarnings("all")
    public void logPoint() {
    }

    @Before("logPoint()")
    public void beforeService(JoinPoint joinPoint) {
        System.out.println("before log");
        LOG.info("before log");
    }

    @After("logPoint()")
    public void afterService(JoinPoint joinPoint) {
        System.out.println("after log");
        LOG.info("after log");
    }
}
