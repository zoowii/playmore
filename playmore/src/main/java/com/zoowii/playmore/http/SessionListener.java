package com.zoowii.playmore.http;

import com.zoowii.playmore.DispatcherServlet;
import com.zoowii.playmore.http.impl.DefaultSessionListener;
import zuice.BeanFactory;

import javax.servlet.http.*;

/**
 * Created by zoowii on 15/2/8.
 */
public class SessionListener implements HttpSessionListener {
    private final ISessionListener sessionListener;

    public SessionListener() {
        super();
        BeanFactory beanFactory = DispatcherServlet.getInjector();
        ISessionListener sessionListenerFromFactory = beanFactory.getBean(ISessionListener.class);
        if (sessionListenerFromFactory == null) {
            this.sessionListener = new DefaultSessionListener();
        } else {
            this.sessionListener = sessionListenerFromFactory;
        }
    }

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        sessionListener.sessionCreated(httpSessionEvent);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        sessionListener.sessionDestroyed(httpSessionEvent);
    }

}
