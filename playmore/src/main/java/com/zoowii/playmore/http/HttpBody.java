package com.zoowii.playmore.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by zoowii on 15/1/22.
 */
public class HttpBody {
    private static final Logger LOG = LoggerFactory.getLogger(HttpBody.class);

    private InputStream inputStream;
    private ByteArrayOutputStream bufferedBody = new ByteArrayOutputStream();
    private boolean readed = false;
    private IOException errorWhenGetRequestInputStream = null;
    private static final int READ_BUF_SIZE = 4096; // Bytes

    public HttpBody(HttpServletRequest request) {
        try {
            this.inputStream = request.getInputStream();
        } catch (IOException e) {
            errorWhenGetRequestInputStream = e;
        }
    }

    /**
     * 使用这个方法没有bufferedBody,除非手动再设置回来,否则只能读取一次这个流,所以如果要重复使用,记得保存好读出的内容.一般在读取大body时这样用
     *
     * @return
     * @throws IOException
     */
    public InputStream getInputStream() throws IOException {
        if (errorWhenGetRequestInputStream != null) {
            throw errorWhenGetRequestInputStream;
        }
        return inputStream;
    }

    public byte[] getContent() throws IOException {
        if (readed) {
            return bufferedBody.toByteArray();
        }
        byte[] buf = new byte[READ_BUF_SIZE];
        InputStream in = getInputStream();
        readed = true;
        int n;
        while ((n = in.read(buf)) > 0) {
            bufferedBody.write(buf, 0, n);
        }
        return bufferedBody.toByteArray();
    }
}
