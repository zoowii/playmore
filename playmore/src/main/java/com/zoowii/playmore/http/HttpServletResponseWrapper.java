package com.zoowii.playmore.http;

import zuice.BeanFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Created by zoowii on 15/1/22.
 */
public class HttpServletResponseWrapper {
    private HttpServletResponse httpServletResponse;
    private BeanFactory beanFactory;

    public HttpServletResponseWrapper(HttpServletResponse httpServletResponse) {
        this.httpServletResponse = httpServletResponse;
    }

    public HttpServletResponse getHttpServletResponse() {
        return httpServletResponse;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public PrintWriter getWriter() throws IOException {
        return httpServletResponse.getWriter();
    }

    public ServletOutputStream getOutputStream() throws IOException {
        return httpServletResponse.getOutputStream();
    }
}
