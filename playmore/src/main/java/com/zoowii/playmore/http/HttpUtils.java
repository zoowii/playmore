package com.zoowii.playmore.http;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoowii.playmore.exceptions.RequestDataInValidRuntimeException;
import zuice.utils.ClassUtils;
import zuice.utils.FieldAccessor;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

/**
 * Created by zoowii on 15/1/22.
 */
public class HttpUtils {
    public static final int FORM = 1;
    public static final int MULTIPART_FORM = 2;
    public static final int JSON_PAYLOAD = 3;

    public static HttpBody getBodyFromHttpRequest(HttpServletRequest request) {
        return new HttpBody(request);
    }

    public static <T> T asForm(HttpServletRequest request, Class<? extends T> cls) {
        return (T) createBeanFromHttpRequest(request, getBodyFromHttpRequest(request), cls, FORM);
    }

    public static <T> T asJson(HttpServletRequest request, Class<? extends T> cls) {
        return (T) createBeanFromHttpRequest(request, getBodyFromHttpRequest(request), cls, JSON_PAYLOAD);
    }

    public static JSONObject asJsonObject(HttpServletRequest request) {
        return (JSONObject) createBeanFromHttpRequest(request, getBodyFromHttpRequest(request), JSONObject.class, JSON_PAYLOAD);
    }

    public static JSONArray asJsonArray(HttpServletRequest request) {
        return (JSONArray) createBeanFromHttpRequest(request, getBodyFromHttpRequest(request), JSONArray.class, JSON_PAYLOAD);
    }

    public static JSONObject formAsJsonObject(HttpServletRequest request) {
        return (JSONObject) createBeanFromHttpRequest(request, getBodyFromHttpRequest(request), JSONObject.class, FORM);
    }

    public static Object createBeanFromHttpRequest(HttpServletRequest request, HttpBody requestBody, Class<?> cls, int type) {
        return createBeanFromHttpRequest(request, requestBody, cls, type, "UTF-8");
    }

    /**
     * 从http request的body中构造java bean
     *
     * @param requestBody
     * @param cls
     * @return
     */
    public static Object createBeanFromHttpRequest(HttpServletRequest request, HttpBody requestBody, Class<?> cls, int type, String charset) {
        if (type == JSON_PAYLOAD) {
            try {
                byte[] bytes = requestBody.getContent();
                String body = new String(bytes, charset);
                try {
                    return JSON.parseObject(body, cls);
                } catch (Exception e) {
                    return JSON.parseArray(body, cls);
                }
            } catch (IOException e) {
                throw new RequestDataInValidRuntimeException(e);
            }
        } else {
            try {
                if (cls == JSONObject.class) {
                    JSONObject jsonObject = new JSONObject();
                    for (String key : request.getParameterMap().keySet()) {
                        jsonObject.put(key, request.getParameter(key));
                        jsonObject.put(key + "[]", request.getParameterValues(key));
                    }
                    return jsonObject;
                }
                Object bean = cls.newInstance();
                for (String key : request.getParameterMap().keySet()) {
                    try {
                        FieldAccessor fieldAccessor = new FieldAccessor(cls, key);
                        Class<?> propType = fieldAccessor.getPropertyType();
                        if (propType == List.class || propType == Collection.class || propType == ArrayList.class) {
                            List<Object> values = new ArrayList<>();
                            Collections.addAll(values, request.getParameterValues(key));
                            fieldAccessor.setProperty(bean, values);
                        } else if (propType.isArray()) {
                            fieldAccessor.setProperty(bean, request.getParameterValues(key));
                        } else {
                            fieldAccessor.setProperty(bean, ClassUtils.parseToPrimitiveTypeValue(propType, request.getParameter(key)));
                        }
                    } catch (Exception e) {

                    }
                }
                return bean;
            } catch (Exception e) {
                throw new RequestDataInValidRuntimeException(e);
            }
        }
    }

    private static final ThreadLocal<HttpServletRequestWrapper> CURRENT_HTTP_REQUEST = new ThreadLocal<>();

    public static void setCurrentHttpRequest(HttpServletRequestWrapper request) {
        CURRENT_HTTP_REQUEST.set(request);
    }

    public static HttpServletRequestWrapper getCurrentHttpRequest() {
        return CURRENT_HTTP_REQUEST.get();
    }

    public static void removeCurrentHttpRequest() {
        CURRENT_HTTP_REQUEST.remove();
    }
}
