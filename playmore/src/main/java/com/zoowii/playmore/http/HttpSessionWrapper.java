package com.zoowii.playmore.http;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.*;

/**
 * Created by zoowii on 15/2/8.
 */
public class HttpSessionWrapper implements HttpSession {
    protected final HttpSession originSession;
    protected final ISessionListener sessionListener;

    public HttpSessionWrapper(HttpSession originSession, ISessionListener sessionListener) {
        this.originSession = originSession;
        this.sessionListener = sessionListener;
    }

    @Override
    public long getCreationTime() {
        return sessionListener.getCreationTime(this);
    }

    @Override
    public String getId() {
        return originSession.getId();
    }

    @Override
    public long getLastAccessedTime() {
        return sessionListener.getLastAccessedTime(this);
    }

    @Override
    public ServletContext getServletContext() {
        return originSession.getServletContext();
    }

    @Override
    public void setMaxInactiveInterval(int i) {
        originSession.setMaxInactiveInterval(i);
    }

    @Override
    public int getMaxInactiveInterval() {
        return originSession.getMaxInactiveInterval();
    }

    @Override
    public HttpSessionContext getSessionContext() {
        return originSession.getSessionContext();
    }

    @Override
    public Object getAttribute(String s) {
        return sessionListener.attributeGet(this, s);
    }

    @Override
    public Object getValue(String s) {
        return getAttribute(s);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        Set<String> names = sessionListener.attributeNamesGet(this);
        final Iterator<String> iter = names.iterator();
        return new Enumeration<String>() {
            @Override
            public boolean hasMoreElements() {
                return iter.hasNext();
            }

            @Override
            public String nextElement() {
                return iter.next();
            }
        };
    }

    @Override
    public String[] getValueNames() {
        Set<String> names = sessionListener.attributeNamesGet(this);
        List<String> namesList = new ArrayList<>();
        namesList.addAll(names);
        String[] namesArray = new String[names.size()];
        for (int i = 0; i < namesList.size(); ++i) {
            namesArray[i] = namesList.get(i);
        }
        return namesArray;
    }

    @Override
    public void setAttribute(String s, Object o) {
        if (sessionListener.attributeGet(this, s) == null) {
            sessionListener.attributeAdd(this, s, o);
        } else {
            sessionListener.attributeReplace(this, s, o);
        }
    }

    @Override
    public void putValue(String s, Object o) {
        setAttribute(s, o);
    }

    @Override
    public void removeAttribute(String s) {
        sessionListener.attributeRemove(this, s);
    }

    @Override
    public void removeValue(String s) {
        removeAttribute(s);
    }

    @Override
    public void invalidate() {
        sessionListener.invalidate(this);
    }

    @Override
    public boolean isNew() {
        return sessionListener.isNew(this);
    }
}
