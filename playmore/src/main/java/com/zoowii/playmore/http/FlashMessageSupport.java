package com.zoowii.playmore.http;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;

/**
 * Created by zoowii on 15/1/22.
 */
public interface FlashMessageSupport {
    public void preProcessHttpRequest(HttpServletRequest request);

    public void flash(HttpServletRequest request, String key, Object message);

    public Map<String, Object> flashes(HttpServletRequest request);

    public Set<String> flashKeys(HttpServletRequest request);

    public Object flash(HttpServletRequest request, String key);

    public String stringFlash(HttpServletRequest request, String key);
}
