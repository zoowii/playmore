package com.zoowii.playmore.http;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import java.util.Date;
import java.util.Set;

/**
 * Created by zoowii on 15/2/8.
 */
public interface ISessionListener {
    public void sessionCreated(HttpSessionEvent httpSessionEvent);

    public void sessionCreated(HttpSession session);

    public void sessionDestroyed(HttpSessionEvent httpSessionEvent);

    public void attributeAdd(HttpSession session, String name, Object value);

    public void attributeRemove(HttpSession session, String name);

    public void attributeReplace(HttpSession session, String name, Object value);

    public Object attributeGet(HttpSession session, String name);

    public Set<String> attributeNamesGet(HttpSession session);

    public void invalidate(HttpSession session);

    public boolean isNew(HttpSession session);

    public void setCreationTime(HttpSession session, Date date);

    public long getCreationTime(HttpSession session);

    public void afterAccessed(HttpSession session);

    public long getLastAccessedTime(HttpSession session);
}
