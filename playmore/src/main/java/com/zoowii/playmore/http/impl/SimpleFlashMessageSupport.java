package com.zoowii.playmore.http.impl;

import com.zoowii.playmore.http.FlashMessageSupport;
import com.zoowii.playmore.http.HttpServletRequestWrapper;
import com.zoowii.playmore.http.HttpSessionWrapper;
import com.zoowii.playmore.http.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by zoowii on 15/1/22.
 */
@Component
public class SimpleFlashMessageSupport implements FlashMessageSupport {
    private static final Logger LOG = LoggerFactory.getLogger(SimpleFlashMessageSupport.class);
    private static final String FLASH_MESSAGE_REQUEST_ATTRIBUTE = "__playmore_flash_message__" + UUID.randomUUID().toString();
    private static final String FLASH_MESSAGE_REQUESTS_HOLDER_ATTRIBUTE = "__playmore_flash_message_requests_holder_attribute__" + UUID.randomUUID().toString();

    private HttpSessionWrapper getCurrentSession(HttpServletRequest request) {
        HttpServletRequestWrapper requestWrapper = HttpUtils.getCurrentHttpRequest();
        if(requestWrapper == null) {
            return null;
        }
        return (HttpSessionWrapper) requestWrapper.getSession(true);
    }

    @Override
    public void preProcessHttpRequest(HttpServletRequest request) {
        HttpSessionWrapper session = getCurrentSession(request);
        if(session == null) {
            return;
        }
        if(session.getAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE) == null) {
            Map<String, Object> map = new HashMap<>();
            map.put(FLASH_MESSAGE_REQUESTS_HOLDER_ATTRIBUTE, new ArrayList<HttpServletRequest>());
            session.setAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE, map);
        }
        Map<String, Object> map = (Map<String, Object>) session.getAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE);
        List<HttpServletRequest> requests = (List<HttpServletRequest>) map.get(FLASH_MESSAGE_REQUESTS_HOLDER_ATTRIBUTE);
        boolean containsThis = requests.contains(request);
        if(requests.size()>2 || (!containsThis && requests.size()>1)) {
            session.removeAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE);
            preProcessHttpRequest(request);
            return;
        }
        if(!requests.contains(request)) {
            requests.add(request);
        }
    }

    @Override
    public void flash(HttpServletRequest request, String key, Object message) {
        preProcessHttpRequest(request);
        HttpSessionWrapper session = getCurrentSession(request);
        if(session == null) {
            return;
        }
        try {
            Map<String, Object> msgs = (Map<String, Object>) session.getAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE);
            msgs.put(key, message);
        } catch (Exception e) {
            LOG.error("io error when flash", e);
        }
    }

    @Override
    public Map<String, Object> flashes(HttpServletRequest request) {
        preProcessHttpRequest(request);
        HttpSessionWrapper session = getCurrentSession(request);
        if(session == null) {
            return new HashMap<>();
        }
        try {
            return (Map<String, Object>) session.getAttribute(FLASH_MESSAGE_REQUEST_ATTRIBUTE);
        } catch (Exception e) {
            LOG.error("io error", e);
            return new HashMap<>();
        }
    }

    @Override
    public Set<String> flashKeys(HttpServletRequest request) {
        return flashes(request).keySet();
    }

    @Override
    public Object flash(HttpServletRequest request, String key) {
        return flashes(request).get(key);
    }

    @Override
    public String stringFlash(HttpServletRequest request, String key) {
        Object val = flash(request, key);
        return val != null ? val.toString() : null;
    }
}
