package com.zoowii.playmore.http.impl;

import com.zoowii.playmore.http.ISessionListener;
import zuice.annotations.Component;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import java.util.*;

/**
 * Created by zoowii on 15/2/8.
 */
@Component
public class DefaultSessionListener implements ISessionListener {
    private static final Map<String, Map<String, Object>> sessions = new HashMap<>();

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        sessions.put(httpSessionEvent.getSession().getId(), new HashMap<String, Object>());
    }

    private synchronized Map<String, Object> getSessionMap(String id) {
        if (sessions.get(id) == null) {
            sessions.put(id, new HashMap<String, Object>());
        }
        return sessions.get(id);
    }

    @Override
    public void sessionCreated(HttpSession session) {
        setCreationTime(session, new Date());
        getSessionMap(session.getId()).put("__is_new__", true);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        sessions.remove(httpSessionEvent.getSession().getId());
    }

    @Override
    public void attributeAdd(HttpSession session, String name, Object value) {
        getSessionMap(session.getId()).put(name, value);
        afterAccessed(session);
    }

    @Override
    public void attributeRemove(HttpSession session, String name) {
        getSessionMap(session.getId()).remove(name);
        afterAccessed(session);
    }

    @Override
    public void attributeReplace(HttpSession session, String name, Object value) {
        getSessionMap(session.getId()).put(name, value);
        afterAccessed(session);
    }

    @Override
    public Object attributeGet(HttpSession session, String name) {
        return getSessionMap(session.getId()).get(name);
    }

    @Override
    public Set<String> attributeNamesGet(HttpSession session) {
        try {
            return getSessionMap(session.getId()).keySet();
        } finally {
            afterAccessed(session);
        }
    }

    @Override
    public void invalidate(HttpSession session) {
        getSessionMap(session.getId()).clear();
        afterAccessed(session);
    }

    @Override
    public boolean isNew(HttpSession session) {
        return (Boolean) getSessionMap(session.getId()).get("__is_new__");
    }

    @Override
    public void setCreationTime(HttpSession session, Date date) {
        session.setAttribute("__creation_time__", date.getTime() + "");
    }

    @Override
    public long getCreationTime(HttpSession session) {
        try {
            return Long.valueOf(session.getAttribute("__creation_time__").toString());
        } catch (Exception e) {
            return new Date().getTime();
        }
    }

    @Override
    public void afterAccessed(HttpSession session) {
        getSessionMap(session.getId()).put("__last_accessed_time__", new Date().getTime());
        getSessionMap(session.getId()).put("__is_new__", false);
    }

    @Override
    public long getLastAccessedTime(HttpSession session) {
        return (Long) getSessionMap(session.getId()).get("__last_accessed_time__");
    }
}
