package com.zoowii.playmore.http;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zoowii.playmore.http.impl.DefaultSessionListener;
import com.zoowii.playmore.http.impl.SimpleFlashMessageSupport;
import com.zoowii.playmore.route.HttpRouteTable;
import com.zoowii.playmore.route.IHttpRouterLoader;
import zuice.BeanFactory;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;

/**
 * Created by zoowii on 15/1/22.
 */
public class HttpServletRequestWrapper implements HttpServletRequest {
    private HttpServletRequest httpServletRequest;
    private BeanFactory beanFactory;
    private FlashMessageSupport flashMessageSupport;
    private HttpRouteTable httpRouteTable;
    private ISessionListener sessionListener;
    private HttpSession httpSession;

    public HttpServletRequestWrapper(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    public void setBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
        flashMessageSupport = beanFactory.getBean(FlashMessageSupport.class);
        if (flashMessageSupport == null) {
            flashMessageSupport = new SimpleFlashMessageSupport();
        }
        httpRouteTable = beanFactory.getBean(IHttpRouterLoader.class).getRouteTable();
        sessionListener = beanFactory.getBean(ISessionListener.class);
        if (sessionListener == null) {
            sessionListener = new DefaultSessionListener();
        }
        this.httpSession = new HttpSessionWrapper(getHttpServletRequest().getSession(), sessionListener);
    }

    public void flash(String key, Object value) {
        flashMessageSupport.flash(httpServletRequest, key, value);
    }

    public Object flash(String key) {
        return flashMessageSupport.flash(httpServletRequest, key);
    }

    public String strFlash(String key) {
        return flashMessageSupport.stringFlash(httpServletRequest, key);
    }

    public Set<String> flashKeys() {
        return flashMessageSupport.flashKeys(httpServletRequest);
    }

    public HttpBody getBody() {
        return HttpUtils.getBodyFromHttpRequest(httpServletRequest);
    }

    public <T> T asForm(Class<? extends T> cls) {
        return HttpUtils.asForm(httpServletRequest, cls);
    }

    public <T> T asJson(Class<? extends T> cls) {
        return HttpUtils.asJson(httpServletRequest, cls);
    }

    public JSONObject asJsonObject() {
        return HttpUtils.asJsonObject(httpServletRequest);
    }

    public JSONArray asJsonArray() {
        return HttpUtils.asJsonArray(httpServletRequest);
    }

    public JSONObject formAsJsonObject() {
        return HttpUtils.formAsJsonObject(httpServletRequest);
    }

    public Object createBeanFromHttpRequest(HttpBody requestBody, Class<?> cls, int type) {
        return HttpUtils.createBeanFromHttpRequest(httpServletRequest, requestBody, cls, type);
    }

    public Object createBeanFromHttpRequest(HttpBody requestBody, Class<?> cls, int type, String charset) {
        return HttpUtils.createBeanFromHttpRequest(httpServletRequest, requestBody, cls, type, charset);
    }

    public String urlFor(Class<?> cls, String methodName, Object... params) {
        return httpRouteTable.urlFor(cls, methodName, params);
    }

    @Override
    public String getMethod() {
        return getHttpServletRequest().getMethod();
    }

    @Override
    public String getPathInfo() {
        return getHttpServletRequest().getPathInfo();
    }

    @Override
    public String getPathTranslated() {
        return getHttpServletRequest().getPathTranslated();
    }

    @Override
    public String getContextPath() {
        return getHttpServletRequest().getContextPath();
    }

    @Override
    public String getQueryString() {
        return getHttpServletRequest().getQueryString();
    }

    @Override
    public String getRemoteUser() {
        return getHttpServletRequest().getRemoteUser();
    }

    @Override
    public boolean isUserInRole(String s) {
        return getHttpServletRequest().isUserInRole(s);
    }

    @Override
    public Principal getUserPrincipal() {
        return getHttpServletRequest().getUserPrincipal();
    }

    @Override
    public String getRequestedSessionId() {
        return getHttpServletRequest().getRequestedSessionId();
    }

    @Override
    public String getRequestURI() {
        return getHttpServletRequest().getRequestURI();
    }

    @Override
    public StringBuffer getRequestURL() {
        return getHttpServletRequest().getRequestURL();
    }

    @Override
    public String getServletPath() {
        return getHttpServletRequest().getServletPath();
    }

    @Override
    public HttpSession getSession(boolean b) {
        return httpSession;
    }

    @Override
    public HttpSession getSession() {
        return httpSession;
    }

    @Override
    public String changeSessionId() {
        String id = getHttpServletRequest().changeSessionId();
        this.httpSession = new HttpSessionWrapper(getHttpServletRequest().getSession(), sessionListener);
        return id;
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        return getHttpServletRequest().isRequestedSessionIdValid();
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return getHttpServletRequest().isRequestedSessionIdFromCookie();
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        return getHttpServletRequest().isRequestedSessionIdFromURL();
    }

    @Override
    public boolean isRequestedSessionIdFromUrl() {
        return getHttpServletRequest().isRequestedSessionIdFromUrl();
    }

    @Override
    public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException {
        return getHttpServletRequest().authenticate(httpServletResponse);
    }

    @Override
    public void login(String s, String s1) throws ServletException {
        getHttpServletRequest().login(s, s1);
    }

    @Override
    public void logout() throws ServletException {
        getHttpServletRequest().logout();
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        return getHttpServletRequest().getParts();
    }

    @Override
    public Part getPart(String s) throws IOException, ServletException {
        return getHttpServletRequest().getPart(s);
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> aClass) throws IOException, ServletException {
        return getHttpServletRequest().upgrade(aClass);
    }

    @Override
    public Object getAttribute(String s) {
        return getHttpServletRequest().getAttribute(s);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return getHttpServletRequest().getAttributeNames();
    }

    @Override
    public String getCharacterEncoding() {
        return getHttpServletRequest().getCharacterEncoding();
    }

    @Override
    public void setCharacterEncoding(String s) throws UnsupportedEncodingException {
        getHttpServletRequest().setCharacterEncoding(s);
    }

    @Override
    public int getContentLength() {
        return getHttpServletRequest().getContentLength();
    }

    @Override
    public long getContentLengthLong() {
        return getHttpServletRequest().getContentLengthLong();
    }

    @Override
    public String getContentType() {
        return getHttpServletRequest().getContentType();
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return getHttpServletRequest().getInputStream();
    }

    @Override
    public String getParameter(String key) {
        return getHttpServletRequest().getParameter(key);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return getHttpServletRequest().getParameterNames();
    }

    @Override
    public String[] getParameterValues(String s) {
        return getHttpServletRequest().getParameterValues(s);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return getHttpServletRequest().getParameterMap();
    }

    @Override
    public String getProtocol() {
        return getHttpServletRequest().getProtocol();
    }

    @Override
    public String getScheme() {
        return getHttpServletRequest().getScheme();
    }

    @Override
    public String getServerName() {
        return getHttpServletRequest().getServerName();
    }

    @Override
    public int getServerPort() {
        return getHttpServletRequest().getServerPort();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return getHttpServletRequest().getReader();
    }

    @Override
    public String getRemoteAddr() {
        return getHttpServletRequest().getRemoteAddr();
    }

    @Override
    public String getRemoteHost() {
        return getHttpServletRequest().getRemoteHost();
    }

    @Override
    public void setAttribute(String s, Object o) {
        getHttpServletRequest().setAttribute(s, o);
    }

    @Override
    public void removeAttribute(String s) {
        getHttpServletRequest().removeAttribute(s);
    }

    @Override
    public Locale getLocale() {
        return getHttpServletRequest().getLocale();
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return getHttpServletRequest().getLocales();
    }

    @Override
    public boolean isSecure() {
        return getHttpServletRequest().isSecure();
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String s) {
        return getHttpServletRequest().getRequestDispatcher(s);
    }

    @Override
    public String getRealPath(String s) {
        return getHttpServletRequest().getRealPath(s);
    }

    @Override
    public int getRemotePort() {
        return getHttpServletRequest().getRemotePort();
    }

    @Override
    public String getLocalName() {
        return getHttpServletRequest().getLocalName();
    }

    @Override
    public String getLocalAddr() {
        return getHttpServletRequest().getLocalAddr();
    }

    @Override
    public int getLocalPort() {
        return getHttpServletRequest().getLocalPort();
    }

    @Override
    public ServletContext getServletContext() {
        return getHttpServletRequest().getServletContext();
    }

    @Override
    public AsyncContext startAsync() throws IllegalStateException {
        return getHttpServletRequest().startAsync();
    }

    @Override
    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
        return getHttpServletRequest().startAsync(servletRequest, servletResponse);
    }

    @Override
    public boolean isAsyncStarted() {
        return getHttpServletRequest().isAsyncStarted();
    }

    @Override
    public boolean isAsyncSupported() {
        return getHttpServletRequest().isAsyncSupported();
    }

    @Override
    public AsyncContext getAsyncContext() {
        return getHttpServletRequest().getAsyncContext();
    }

    @Override
    public DispatcherType getDispatcherType() {
        return getHttpServletRequest().getDispatcherType();
    }

    @Override
    public String getAuthType() {
        return getHttpServletRequest().getAuthType();
    }

    @Override
    public Cookie[] getCookies() {
        return getHttpServletRequest().getCookies();
    }

    @Override
    public long getDateHeader(String s) {
        return getHttpServletRequest().getDateHeader(s);
    }

    @Override
    public String getHeader(String s) {
        return getHttpServletRequest().getHeader(s);
    }

    @Override
    public Enumeration<String> getHeaders(String s) {
        return getHttpServletRequest().getHeaders(s);
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return getHttpServletRequest().getHeaderNames();
    }

    @Override
    public int getIntHeader(String s) {
        return getHttpServletRequest().getIntHeader(s);
    }
}
