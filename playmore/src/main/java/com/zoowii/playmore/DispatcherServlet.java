package com.zoowii.playmore;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.IExceptionHandler;
import com.zoowii.playmore.action.IRouteNotFoundExceptionHandler;
import com.zoowii.playmore.action.impl.DefaultExceptionHandler;
import com.zoowii.playmore.action.impl.DefaultRouteNotFoundExceptionHandler;
import com.zoowii.playmore.exceptions.ActionResultReturnRuntimeException;
import com.zoowii.playmore.exceptions.RouteNotFoundException;
import com.zoowii.playmore.http.FlashMessageSupport;
import com.zoowii.playmore.route.IHttpRouterLoader;
import com.zoowii.playmore.route.RouteInfo;
import com.zoowii.playmore.route.impl.HttpRouterLoader;
import org.apache.commons.lang3.tuple.Pair;
import zuice.BeanFactory;
import zuice.config.xml.ClassPathXmlConfigLoader;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by zoowii on 2015/1/15.
 */
public class DispatcherServlet extends HttpServlet {

    private static BeanFactory injector = ClassPathXmlConfigLoader.createInjector("playmore.xml");

    public static BeanFactory getInjector() {
        return injector;
    }

    @Override
    public void service(HttpServletRequest request,
                        HttpServletResponse response) {
        if (request.getParameter("__debug") != null) { // FIXME: for debug
            injector = ClassPathXmlConfigLoader.createInjector("playmore.xml");
        }
        HttpRouterLoader.setWebContextPath(request.getContextPath());
        try {
            IHttpRouterLoader httpRouterLoader = injector.getBean(IHttpRouterLoader.class);
            String httpMethod = request.getMethod();
            ActionResult actionResult;
            try {
                actionResult = httpRouterLoader.getRouteTable().routeAndCall(httpMethod, request.getRequestURI(), request, response);
            } catch (ActionResultReturnRuntimeException e) {
                actionResult = e.getActionResult();
            }
            if (actionResult == null) {
                // actionResult为null表示程序已经在action中确定处理好了输出（也许是直接使用HttpServletResponse输出）
                return;
            }
            actionResult.render(response);
        } catch (RouteNotFoundException e) {
            IRouteNotFoundExceptionHandler routeNotFoundExceptionHandler = injector.getBean(IRouteNotFoundExceptionHandler.class);
            if (routeNotFoundExceptionHandler == null) {
                routeNotFoundExceptionHandler = new DefaultRouteNotFoundExceptionHandler();
            }
            routeNotFoundExceptionHandler.handleException(request, response, e);
        } catch (Exception e) {
            e.printStackTrace();
            IExceptionHandler exceptionHandler = injector.getBean(IExceptionHandler.class);
            if (exceptionHandler == null) {
                exceptionHandler = new DefaultExceptionHandler();
            }
            exceptionHandler.handleException(request, response, e);
        }
    }
}
