package com.zoowii.playmore.util;

import com.google.common.base.Function;
import com.zoowii.playmore.exceptions.CloneFailedRuntimeException;
import org.apache.commons.beanutils.BeanUtils;

import java.util.*;

/**
 * Created by zoowii on 2015/1/16.
 */
public class ListUtils {
    public static <T, R> List<R> map(T[] source, Function<T, R> fn) {
        if (source == null) {
            return null;
        }
        return map(Arrays.asList(source), fn);
    }

    public static <T> List<T> cloneItems(List<T> source) {
        if (source == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (T item : source) {
            try {
                T copied = (T) BeanUtils.cloneBean(item);
                result.add(copied);
            } catch (Exception e) {
                throw new CloneFailedRuntimeException(e);
            }
        }
        return result;
    }

    public static <T, R> List<R> map(List<T> source, Function<T, R> fn) {
        if (source == null) {
            return null;
        }
        List<R> result = new ArrayList<>();
        if (fn == null) {
            return result;
        }
        for (T item : source) {
            result.add(fn.apply(item));
        }
        return result;
    }

    public static <T> List<T> compactOnce(List<List<T>> source) {
        if (source == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (List<T> item : source) {
            if (item != null) {
                result.addAll(item);
            }
        }
        return result;
    }

    /**
     * 压缩列表，排除其中的null值
     *
     * @param source
     * @param <T>
     * @return
     */
    public static <T> List<T> compactNull(List<T> source) {
        if (source == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (T item : source) {
            if (item != null) {
                result.add(item);
            }
        }
        return result;
    }

    public static <T> List<T> filter(T[] source, Function<T, Boolean> fn) {
        if (source == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        if (fn == null) {
            Collections.addAll(result, source);
            return result;
        }
        for (T item : source) {
            Boolean fnResult = fn.apply(item);
            if (fnResult != null && fnResult) {
                result.add(item);
            }
        }
        return result;
    }
}
