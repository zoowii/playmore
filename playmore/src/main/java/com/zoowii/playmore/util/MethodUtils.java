package com.zoowii.playmore.util;

import com.zoowii.playmore.exceptions.ActionResultReturnRuntimeException;
import com.zoowii.playmore.exceptions.ActionUsefulRuntimeException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by zoowii on 2015/1/17.
 */
public class MethodUtils {
    /**
     * @param instance 可能是实际类型的代理类
     * @param method   可能不是instance.getClass()中的方法，而且它的代理类的方法
     * @param params
     * @return 调用方法的返回值
     */
    public static Object callMethod(Object instance, Method method, Object... params) throws InvocationTargetException, IllegalAccessException {
        if (Proxy.isProxyClass(instance.getClass())) {
            try {
                return Proxy.getInvocationHandler(instance).invoke(instance, method, params);
            } catch (Throwable throwable) {
                if (throwable.getCause() instanceof RuntimeException) {
                    throw (RuntimeException) throwable.getCause();
                }
                throw new InvocationTargetException(throwable);
            }
        } else {
            return method.invoke(instance, params);
        }
    }
}
