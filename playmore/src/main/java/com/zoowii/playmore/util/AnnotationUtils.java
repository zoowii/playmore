package com.zoowii.playmore.util;

import org.apache.commons.lang.ClassUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by zoowii on 2015/1/16.
 */
public class AnnotationUtils {
    public static <T> T getAnnotation(Class<?> cls, Class<T> annotationCls) {
        if (cls == null || annotationCls == null) {
            return null;
        }
        for (Annotation annotation : cls.getAnnotations()) {
            if (ClassUtils.isAssignable(annotation.getClass(), annotationCls)) {
                return (T) annotation;
            }
        }
        return null;
    }

    public static <T> T getAnnotation(Method method, Class<T> annotationCls) {
        if (method == null || annotationCls == null) {
            return null;
        }
        for (Annotation annotation : method.getAnnotations()) {
            if (ClassUtils.isAssignable(annotation.getClass(), annotationCls)) {
                return (T) annotation;
            }
        }
        return null;
    }
}
