package com.zoowii.playmore.util;

import org.apache.commons.lang.ClassUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zoowii on 2015/1/16.
 */
public class ObjectUtils {
    /**
     * 将对象（很可能是字符串或者基本数据类型）转换成cls表示的基本数据类型或者常见基础类型(String, Date等)
     *
     * @param value
     * @param cls
     * @return
     */
    public static Object parseBaseType(Object value, Class<?> cls) {
        if (value == null || cls == null) {
            return value;
        }
        if (cls == Integer.class || cls.getName().equals("int")) {
            return Integer.valueOf(value.toString());
        } else if (cls == Boolean.class || cls.getName().equals("boolean")) {
            return java.lang.Boolean.valueOf(value.toString());
        } else if (cls == Double.class || cls.getName().equals("double")) {
            return java.lang.Double.valueOf(value.toString());
        } else if (cls == Float.class || cls.getName().equals("float")) {
            return java.lang.Float.valueOf(value.toString());
        } else if (cls == Date.class) {
            if (value.getClass() == Integer.class || value.getClass() == Long.class) {
                return new Date((Long) parseBaseType(value, Long.class));
            }
            SimpleDateFormat sdf = new SimpleDateFormat();
            try {
                return sdf.parse(value.toString());
            } catch (ParseException e) {
                return value;
            }
        } else {
            return value;
        }
    }
}
