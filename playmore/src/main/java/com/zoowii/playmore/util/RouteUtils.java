package com.zoowii.playmore.util;

import com.zoowii.jpa_utils.util.StringUtil;
import com.zoowii.playmore.route.RouteInfo;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zoowii on 2015/1/16.
 */
public class RouteUtils {
    public static List<RouteInfo> getRouteInfosOfControllerClass(Class<?> controllerCls) {
        List<RouteInfo> result = new ArrayList<>();
        if (controllerCls == null) {
            return result;
        }
        Method[] methods = controllerCls.getDeclaredMethods();
        for (Method method : methods) {
            com.zoowii.playmore.annotation.Route routeAnno = AnnotationUtils.getAnnotation(method, com.zoowii.playmore.annotation.Route.class);
            if (routeAnno == null) {
                continue;
            }
            for (String route : routeAnno.value()) {
                RouteInfo routeInfo = new RouteInfo();
                routeInfo.setCls(controllerCls);
                routeInfo.setMethod(method);
                routeInfo.setMethods(routeAnno.methods());
                routeInfo.setRoute(route);
                result.add(routeInfo);
            }
        }
        return result;
    }

    public static final String[] availableActionMethods = {"get", "post", "put", "delete", "head", "option", "any"};

    public static List<RouteInfo> getRouteInfosOfActionClass(Class<?> actionCls) {
        List<RouteInfo> result = new ArrayList<>();
        if (actionCls == null) {
            return result;
        }
        Method[] methods = actionCls.getDeclaredMethods();
        for (Method method : methods) {
            if (Arrays.asList(availableActionMethods).contains(method.getName())) {
                RouteInfo routeInfo = new RouteInfo();
                routeInfo.setCls(actionCls);
                routeInfo.setMethod(method);
                String[] supportHttpMethods = {method.getName().toLowerCase()};
                routeInfo.setMethods(supportHttpMethods);
                routeInfo.setRoute("");
                result.add(routeInfo);
            }
        }
        return result;
    }
}
