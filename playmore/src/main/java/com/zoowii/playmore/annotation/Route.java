package com.zoowii.playmore.annotation;

import com.zoowii.janyroute.http.HttpAnyRoute;
import com.zoowii.janyroute.http.HttpRoute;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by zoowii on 2015/1/15.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Route {
    public static final String GET = HttpAnyRoute.GET_METHOD;
    public static final String POST = HttpAnyRoute.POST_METHOD;
    public static final String PUT = HttpAnyRoute.PUT_METHOD;
    public static final String DELETE = HttpAnyRoute.DELETE_METHOD;
    public static final String HEAD = HttpAnyRoute.HEAD_METHOD;
    public static final String OPTION = HttpAnyRoute.OPTION_METHOD;
    public static final String ANY = HttpAnyRoute.ANY_METHOD;

    public String[] value() default {""};

    public String[] methods() default {GET};
}
