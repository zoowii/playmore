package com.zoowii.playmore.route;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.exceptions.RouteNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zoowii on 2015/1/15.
 */
public interface IHttpRouterLoader {
    public HttpRouteTable getRouteTable();
}
