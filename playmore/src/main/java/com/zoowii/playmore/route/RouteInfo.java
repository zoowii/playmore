package com.zoowii.playmore.route;

import com.zoowii.janyroute.core.RouteParamBinding;
import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.ActionResults;
import com.zoowii.playmore.action.IAction;
import com.zoowii.playmore.exceptions.RouteHandlerInvokeException;
import com.zoowii.playmore.http.HttpServletRequestWrapper;
import com.zoowii.playmore.http.HttpServletResponseWrapper;
import com.zoowii.playmore.http.HttpUtils;
import com.zoowii.playmore.util.MethodUtils;
import com.zoowii.playmore.util.ObjectUtils;
import org.apache.commons.lang.ClassUtils;
import zuice.BeanFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zoowii on 2015/1/16.
 */
public class RouteInfo {
    private Class<?> cls;
    private Method method;
    private String route;
    private String[] methods;

    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String[] getMethods() {
        return methods;
    }

    public void setMethods(String[] methods) {
        this.methods = methods;
    }

    public boolean isController() {
        return !isAction();
    }

    public boolean isAction() {
        return ClassUtils.isAssignable(cls, IAction.class);
    }

    public String getAnyRouteName(String httpMethod) {
        return getRouteName(cls, method.getName(), httpMethod);
    }

    public static String getRouteName(Class<?> cls, String methodName, String httpMethod) {
        return String.format("%s@%s@%s", httpMethod, cls.getCanonicalName(), methodName); // FIXME: whether the method's name will change if after proxy?
    }

    public static final String HTTP_SERVLET_REQUEST_ATTRIBUTE_HOLDER = "ROUTE_INFO";

    public ActionResult call(com.zoowii.janyroute.core.Route route, HttpServletRequest request, HttpServletResponse response, BeanFactory beanFactory) {
        // 方法的前两个参数如果是HttpServletRequest和/或HttpServletResponse，则注入，否则按数量注入bindings中的数据
        // TODO: 这部分判断规则的可以考虑cache
        request.setAttribute(HTTP_SERVLET_REQUEST_ATTRIBUTE_HOLDER, route);
        List<Object> parameters = new ArrayList<>();
        Class<?>[] parameterTypes = method.getParameterTypes();
        HttpServletRequestWrapper wrapperRequest = new HttpServletRequestWrapper(request);
        wrapperRequest.setBeanFactory(beanFactory);
        HttpServletResponseWrapper wrapperResponse = new HttpServletResponseWrapper(response);
        wrapperResponse.setBeanFactory(beanFactory);
        HttpUtils.setCurrentHttpRequest(wrapperRequest);
        if (parameterTypes.length > 0) {
            if (parameterTypes[0] == HttpServletRequest.class) {
                parameters.add(request);
            }
            if (parameterTypes[0] == HttpServletResponse.class) {
                parameters.add(response);
            }
            if (parameterTypes[0] == HttpServletRequestWrapper.class) {
                parameters.add(wrapperRequest);
            }
            if (parameterTypes[0] == HttpServletResponseWrapper.class) {
                parameters.add(wrapperResponse);
            }
        }
        if (parameterTypes.length > 1) {
            if (parameterTypes[1] == HttpServletRequest.class) {
                parameters.add(request);
            }
            if (parameterTypes[1] == HttpServletResponse.class) {
                parameters.add(response);
            }
            if (parameterTypes[1] == HttpServletRequestWrapper.class) {
                parameters.add(wrapperRequest);
            }
            if (parameterTypes[1] == HttpServletResponseWrapper.class) {
                parameters.add(wrapperResponse);
            }
        }
        int remainingParamsCount = parameterTypes.length - parameters.size();
        int preParametersCount = parameters.size();
        if (remainingParamsCount > 0) {
            List<RouteParamBinding> routeParamBindings = route.getBindings().subList(0, remainingParamsCount);
            remainingParamsCount = remainingParamsCount > routeParamBindings.size() ? routeParamBindings.size() : remainingParamsCount;
            for (int i = 0; i < remainingParamsCount; ++i) {
                RouteParamBinding routeParamBinding = routeParamBindings.get(i);
                Class<?> parameterType = parameterTypes[preParametersCount + i];
                Object param = ObjectUtils.parseBaseType(routeParamBinding.getValue(), parameterType);
                parameters.add(param);
            }
        }
        Object handlerObj = beanFactory.getBean(cls);
        try {
            Object handlerRes = MethodUtils.callMethod(handlerObj, method, parameters.toArray());
            return ActionResults.ok(handlerRes);
        } catch (IllegalAccessException e) {
            throw new RouteHandlerInvokeException(e);
        } catch (InvocationTargetException e) {
            throw new RouteHandlerInvokeException(e);
        } finally {
            HttpUtils.removeCurrentHttpRequest();
        }
    }
}
