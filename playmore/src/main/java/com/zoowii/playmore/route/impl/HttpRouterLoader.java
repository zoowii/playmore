package com.zoowii.playmore.route.impl;

import com.google.common.base.Function;
import com.zoowii.janyroute.core.Route;
import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.annotation.Action;
import com.zoowii.playmore.annotation.Controller;
import com.zoowii.playmore.exceptions.RouteNotFoundException;
import com.zoowii.playmore.route.HttpRouteTable;
import com.zoowii.playmore.route.IHttpRouterLoader;
import com.zoowii.playmore.route.RouteInfo;
import com.zoowii.playmore.util.AnnotationUtils;
import com.zoowii.playmore.util.ListUtils;
import com.zoowii.playmore.util.RouteUtils;
import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.BeanFactory;
import zuice.annotations.Autowired;
import zuice.annotations.Component;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zoowii on 2015/1/15.
 */
@Component
@Singleton
public class HttpRouterLoader implements IHttpRouterLoader {
    private static String webContextPath = "";
    private static final Logger LOG = LoggerFactory.getLogger(HttpRouterLoader.class);
    private HttpRouteTable httpRouteTable;
    private List<Class<?>> controllerClassList = new ArrayList<>();
    private List<Class<?>> actionClassList = new ArrayList<>();

    @Autowired
    private BeanFactory beanFactory;

    public static String getWebContextPath() {
        return webContextPath;
    }

    public static void setWebContextPath(String webContextPath) {
        HttpRouterLoader.webContextPath = webContextPath;
    }

    @Inject
    public HttpRouterLoader(BeanFactory beanFactory) {
        ListUtils.map(beanFactory.getBeanClasses(), new Function<Class<?>, Object>() {
            @Nullable
            @Override
            public Object apply(Class<?> cls) {
                Annotation[] annotations = cls.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (ClassUtils.isAssignable(annotation.getClass(), Controller.class)) {
                        controllerClassList.add(cls);
                        return null;
                    }
                    if (ClassUtils.isAssignable(annotation.getClass(), Action.class)) {
                        actionClassList.add(cls);
                        return null;
                    }
                }
                return null;
            }
        });
    }

    public void init() {
        LOG.info("initing http router loader");
        httpRouteTable = new HttpRouteTable(beanFactory);
        try {
            for (Class<?> controllerCls : controllerClassList) {
                com.zoowii.playmore.annotation.Route routeAnno = AnnotationUtils.getAnnotation(controllerCls, com.zoowii.playmore.annotation.Route.class);
                if (routeAnno == null) {
                    continue;
                }
                String[] contexts = routeAnno.value();
                List<RouteInfo> routeInfos = RouteUtils.getRouteInfosOfControllerClass(controllerCls);
                for (String context : contexts) {
                    httpRouteTable.addContextRoute(context, routeInfos);
                }
            }
            for (Class<?> actionCls : actionClassList) {
                com.zoowii.playmore.annotation.Route routeAnno = AnnotationUtils.getAnnotation(actionCls, com.zoowii.playmore.annotation.Route.class);
                if (routeAnno == null) {
                    continue;
                }
                String[] actionRoutes = routeAnno.value();
                List<RouteInfo> routeInfos = RouteUtils.getRouteInfosOfActionClass(actionCls);
                for (String actionRoute : actionRoutes) {
                    httpRouteTable.addContextRoute(actionRoute, ListUtils.cloneItems(routeInfos));
                }
            }
        } finally {
            httpRouteTable.build(webContextPath);
        }
    }

    /**
     * @return
     */
    public HttpRouteTable getRouteTable() {
        return httpRouteTable;
    }
}
