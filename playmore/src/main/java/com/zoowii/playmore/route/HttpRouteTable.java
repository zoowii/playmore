package com.zoowii.playmore.route;

import com.google.common.base.Function;
import com.zoowii.janyroute.core.AnyRoute;
import com.zoowii.janyroute.core.Route;
import com.zoowii.janyroute.http.HttpAnyRoute;
import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.exceptions.RouteNotFoundException;
import com.zoowii.playmore.util.ListUtils;
import com.zoowii.playmore.util.RouteUtils;
import org.apache.commons.lang3.tuple.Pair;
import zuice.BeanFactory;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zoowii on 2015/1/15.
 */
public class HttpRouteTable {
    private com.zoowii.janyroute.http.HttpRouteTable anyRouteHttpRouteTable;
    private List<Route> anyRoutes = new ArrayList<>();

    private BeanFactory beanFactory;

    public HttpRouteTable(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public void addRoute(RouteInfo routeInfo) {
        if (routeInfo != null) {
            anyRoutes.addAll(getRoutesFromRouteInfo(routeInfo));
        }
    }

    private static List<Route> getRoutesFromRouteInfo(final RouteInfo routeInfo) {
        if (routeInfo == null) {
            return null;
        }
        return ListUtils.map(routeInfo.getMethods(), new Function<String, Route>() {
            @Nullable
            @Override
            public Route apply(@Nullable String httpMethod) {
                if (httpMethod == null) {
                    return null;
                }
                assert HttpAnyRoute.GET_METHOD.toLowerCase().equals(HttpAnyRoute.GET_METHOD); // 确保使用小写字符串来传递http method
                switch (httpMethod.toLowerCase()) { // here requires JDK7
                    case HttpAnyRoute.GET_METHOD: {
                        return HttpAnyRoute.GET(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.POST_METHOD: {
                        return HttpAnyRoute.POST(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.PUT_METHOD: {
                        return HttpAnyRoute.PUT(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.DELETE_METHOD: {
                        return HttpAnyRoute.DELETE(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.HEAD_METHOD: {
                        return HttpAnyRoute.HEAD(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.OPTION_METHOD: {
                        return HttpAnyRoute.OPTION(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    case HttpAnyRoute.ANY_METHOD: {
                        return HttpAnyRoute.ANY(routeInfo.getRoute(), routeInfo, routeInfo.getAnyRouteName(httpMethod));
                    }
                    default: {
                        return null;
                    }
                }
            }
        });
    }

    public void addContextRoute(String context, List<RouteInfo> routeInfos) {
        Route route = AnyRoute.contextRoute(context,
                ListUtils.compactNull(ListUtils.compactOnce(ListUtils.map(
                        routeInfos, new Function<RouteInfo, List<Route>>() {
                            @Nullable
                            @Override
                            public List<Route> apply(final RouteInfo routeInfo) {
                                return getRoutesFromRouteInfo(routeInfo);
                            }
                        }))));
        anyRoutes.add(route);
    }

    public void build() {
        build("");
    }

    public void build(String context) {
        anyRouteHttpRouteTable = HttpAnyRoute.makeRouteTable(context, anyRoutes);
    }

    public com.zoowii.janyroute.http.HttpRouteTable getAnyRouteHttpRouteTable() {
        return anyRouteHttpRouteTable;
    }

    public Pair<RouteInfo, Route> route(String httpMethod, String path) throws RouteNotFoundException {
        Route matched = anyRouteHttpRouteTable.httpMatch(httpMethod, path);
        if (matched == null) {
            throw new RouteNotFoundException(String.format("Can't find route handler of %s %s", httpMethod, path));
        }
        if (!(matched.getHandler() instanceof RouteInfo)) {
            throw new RouteNotFoundException("The handler type of the route must be RouteInfo");
        }
        RouteInfo routeInfo = (RouteInfo) matched.getHandler();
        return Pair.of(routeInfo, matched);
    }

    public ActionResult routeAndCall(String httpMethod, String path, HttpServletRequest request, HttpServletResponse response) throws RouteNotFoundException {
        Pair<RouteInfo, Route> routeInfoRoutePair = route(httpMethod, path);
        RouteInfo routeInfo = routeInfoRoutePair.getLeft();
        return routeInfo.call(routeInfoRoutePair.getRight(), request, response, beanFactory);
    }

    public String urlFor(Class<?> cls, String methodName, Object... params) {
        for (String httpMethod : RouteUtils.availableActionMethods) {
            String routeName = RouteInfo.getRouteName(cls, methodName, httpMethod);
            return anyRouteHttpRouteTable.urlFor(routeName, params);
        }
        return null;
    }
}
