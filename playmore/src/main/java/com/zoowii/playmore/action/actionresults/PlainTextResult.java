package com.zoowii.playmore.action.actionresults;

import com.zoowii.playmore.exceptions.ActionResultRenderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by zoowii on 2015/1/17.
 */
public class PlainTextResult extends OKResult {
    public static final String PLAIN_TEXT_CONTENT_TYPE = "text/plain";
    private static final Logger LOG = LoggerFactory.getLogger(PlainTextResult.class);
    protected String text;

    public PlainTextResult(String text) {
        super();
        this.text = text;
        this.contentType = PLAIN_TEXT_CONTENT_TYPE + "; charset=" + getEncoding();
    }

    public PlainTextResult(String text, String encoding) {
        super();
        setEncoding(encoding);
        this.text = text;
        this.contentType = PLAIN_TEXT_CONTENT_TYPE + "; charset=" + getEncoding();
    }

    @Override
    public InputStream getOutput() throws ActionResultRenderException {
        try {
            return text != null ? new ByteArrayInputStream(text.getBytes(getEncoding())) : null;
        } catch (UnsupportedEncodingException e) {
            LOG.error("unsupported encoding", e);
            return null;
        }
    }
}
