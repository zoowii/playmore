package com.zoowii.playmore.action;

import com.zoowii.playmore.exceptions.ActionResultRenderException;

import javax.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * Created by zoowii on 2015/1/15.
 */
public interface ActionResult {
    public int getStatusCode();

    public ActionResult setStatusCode(int statusCode);

    public String getContentType();

    public ActionResult setContentType(String contentType);

    public String getEncoding();

    public ActionResult setEncoding(String encoding);

    public ActionResult setHeader(String key, Object value);

    public String getHeader(String key);

    public Set<String> getHeaderNames();

    public void render(HttpServletResponse response) throws ActionResultRenderException;
}
