package com.zoowii.playmore.action.impl;

import com.zoowii.playmore.action.IExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by zoowii on 2015/1/17.
 */
@Component
public class DefaultExceptionHandler implements IExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @Override
    public void handleException(HttpServletRequest request, HttpServletResponse response, Throwable throwable) {
        try {
            if (throwable == null || throwable.getMessage() == null) {
                return;
            }
            response.getWriter().write(throwable.getMessage());
        } catch (IOException e) {
            LOG.error("get response writer error", e);
        }
    }
}
