package com.zoowii.playmore.action.actionresults;

import com.zoowii.playmore.exceptions.ActionResultRenderException;

import java.io.InputStream;

/**
 * Created by zoowii on 2015/1/17.
 */
public class InputStreamResult extends OKResult {
    public static final String ANY_BINARY_CONTENT_TYPE = "application/octet-stream";
    private final InputStream inputStream;

    public InputStreamResult(InputStream inputStream) {
        super();
        this.inputStream = inputStream;
        this.setContentType(ANY_BINARY_CONTENT_TYPE);
    }

    @Override
    public InputStream getOutput() throws ActionResultRenderException {
        return inputStream;
    }
}
