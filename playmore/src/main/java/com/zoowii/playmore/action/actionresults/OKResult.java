package com.zoowii.playmore.action.actionresults;

import com.zoowii.playmore.action.AbstractActionResult;
import com.zoowii.playmore.exceptions.ActionResultRenderException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

/**
 * Created by zoowii on 2015/1/17.
 */
public abstract class OKResult extends AbstractActionResult {
    private static final Logger LOG = LoggerFactory.getLogger(OKResult.class);

    public OKResult() {
        this.statusCode = HttpURLConnection.HTTP_OK;
    }

    public abstract InputStream getOutput() throws ActionResultRenderException;

    public void closeOutput(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
                LOG.error("error when close output stream", e);
            }
        }
    }

    @Override
    public void render(HttpServletResponse response) throws ActionResultRenderException {
        response.setStatus(getStatusCode());
        InputStream inputStream = getOutput();
        if (inputStream != null) {
            try {
                response.setCharacterEncoding(getEncoding());
                response.setContentType(getContentType());
                for (String key : getHeaderNames()) {
                    response.setHeader(key, getHeader(key));
                }
                IOUtils.copy(inputStream, response.getOutputStream());
            } catch (IOException e) {
                throw new ActionResultRenderException(e);
            } finally {
                closeOutput(inputStream);
            }
        }
    }
}
