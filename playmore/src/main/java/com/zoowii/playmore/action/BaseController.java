package com.zoowii.playmore.action;

import com.alibaba.fastjson.JSON;
import com.zoowii.janyroute.core.RouteParamBinding;
import com.zoowii.playmore.http.HttpSessionWrapper;
import com.zoowii.playmore.http.HttpUtils;
import com.zoowii.playmore.route.RouteInfo;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by zoowii on 2015/1/17.
 */
public class BaseController {
    public ActionResult ok(String text) {
        return ActionResults.ok(text);
    }

    public ActionResult ok(File file) {
        return ActionResults.ok(file);
    }

    public ActionResult ok(InputStream inputStream) {
        return ActionResults.ok(inputStream);
    }

    public ActionResult ok(Object res) {
        return ActionResults.ok(res);
    }

    public ActionResult redirect(String url) {
        return ActionResults.redirect(url);
    }

    public ActionResult json(Object obj) {
        if (obj == null) {
            return null;
        }
        return ok(JSON.toJSONString(obj));
    }

    public ActionResult text(String text) {
        return ActionResults.text(text);
    }

    public ActionResult text(String text, String encoding) {
        return ActionResults.text(text, encoding);
    }

    public ActionResult file(File file) {
        return ActionResults.file(file);
    }

    public ActionResult file(String filePath, String mode) {
        return ActionResults.file(filePath, mode);
    }

    public ActionResult file(String filePath) {
        return ActionResults.file(filePath);
    }

    public ActionResult stream(InputStream inputStream) {
        return ActionResults.stream(inputStream);
    }

    public ActionResult html(String htmlText) {
        return ActionResults.html(htmlText);
    }

    public ActionResult html(String htmlText, String encoding) {
        return ActionResults.html(htmlText, encoding);
    }

    public ActionResult serveFilesInResource(String... paths) {
        return ActionResults.serveFilesInResource(paths);
    }

    public ActionResult serveFiles(String... paths) {
        return ActionResults.serveFiles(paths);
    }

    public List<RouteParamBinding> getRouteBindings(HttpServletRequest request) {
        com.zoowii.janyroute.core.Route routeInfo = getRouteInfo(request);
        return routeInfo != null ? routeInfo.getBindings() : null;
    }

    public String getRouteName(HttpServletRequest request) {
        com.zoowii.janyroute.core.Route routeInfo = getRouteInfo(request);
        return routeInfo != null ? routeInfo.getName() : null;
    }

    public com.zoowii.janyroute.core.Route getRouteInfo(HttpServletRequest request) {
        Object attribute = request.getAttribute(RouteInfo.HTTP_SERVLET_REQUEST_ATTRIBUTE_HOLDER);
        if (attribute == null || !(attribute instanceof com.zoowii.janyroute.core.Route)) {
            return null;
        }
        return (com.zoowii.janyroute.core.Route) attribute;
    }
    public HttpSessionWrapper session() {
        return (HttpSessionWrapper) HttpUtils.getCurrentHttpRequest().getSession(true);
    }
    public Object session(String key) {
        HttpSessionWrapper sessionWrapper = session();
        if(sessionWrapper == null) {
            return null;
        }
        return sessionWrapper.getAttribute(key);
    }
    public boolean session(String key, Object value) {
        HttpSessionWrapper sessionWrapper = session();
        if(sessionWrapper == null) {
            return false;
        }
        if(value == null) {
            sessionWrapper.removeAttribute(key);
        } else {
            sessionWrapper.setAttribute(key, value);
        }
        return true;
    }

    public String urlFor(Class<?> ctrlCls, String methodName, Object... params) {
        return HttpUtils.getCurrentHttpRequest().urlFor(ctrlCls, methodName, params);
    }
}
