package com.zoowii.playmore.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zoowii on 2015/1/17.
 */
public interface IExceptionHandler {
    public void handleException(HttpServletRequest request, HttpServletResponse response, Throwable throwable);
}
