package com.zoowii.playmore.action.actionresults;

import com.zoowii.playmore.exceptions.ActionResultRenderException;

import java.io.*;

/**
 * Created by zoowii on 2015/1/17.
 */
public class FileResult extends OKResult {
    private final File file;
    private final String filePath;

    public FileResult(File file) {
        super();
        this.file = file;
        this.filePath = file != null ? file.getAbsolutePath() : null;
    }

    public FileResult(String filePath, String mode) {
        if (filePath != null) {
            this.file = new File(filePath, mode);
        } else {
            this.file = null;
        }
        this.filePath = file != null ? file.getAbsolutePath() : null;
    }

    public FileResult(String filePath) {
        this(filePath, "r");
    }

    @Override
    public InputStream getOutput() throws ActionResultRenderException {
        if (file == null) {
            throw new ActionResultRenderException("Can't find file " + filePath);
        }
        try {
            return new BufferedInputStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new ActionResultRenderException(e);
        }
    }
}
