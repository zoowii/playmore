package com.zoowii.playmore.action;

import com.zoowii.playmore.exceptions.RouteNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zoowii on 2015/1/17.
 */
public interface IRouteNotFoundExceptionHandler {
    public void handleException(HttpServletRequest request, HttpServletResponse response, RouteNotFoundException e);
}
