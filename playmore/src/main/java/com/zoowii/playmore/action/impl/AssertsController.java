package com.zoowii.playmore.action.impl;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.action.BaseController;
import com.zoowii.playmore.annotation.Controller;
import com.zoowii.playmore.annotation.Route;

import javax.inject.Singleton;

/**
 * Created by zoowii on 2015/1/17.
 */
@Controller
@Singleton
@Route("/static")
public class AssertsController extends BaseController {
    @Route("/:*path")
    public ActionResult asserts(String path) {
        return serveFilesInResource("static", path);
    }
}
