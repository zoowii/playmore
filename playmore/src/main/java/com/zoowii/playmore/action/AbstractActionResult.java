package com.zoowii.playmore.action;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by zoowii on 2015/1/17.
 */
public abstract class AbstractActionResult implements ActionResult {
    protected String encoding = "UTF-8";
    protected Map<String, String> headers = new HashMap<>();
    protected String contentType = "text/html; encoding=" + encoding;
    protected int statusCode = HttpURLConnection.HTTP_OK;

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public ActionResult setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    @Override
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public ActionResult setStatusCode(int statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public ActionResult setEncoding(String encoding) {
        this.encoding = encoding;
        return this;
    }

    @Override
    public ActionResult setHeader(String key, Object value) {
        headers.put(key, value + "");
        return this;
    }

    @Override
    public String getHeader(String key) {
        return headers.get(key);
    }

    @Override
    public Set<String> getHeaderNames() {
        return headers.keySet();
    }
}
