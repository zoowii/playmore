package com.zoowii.playmore.action.actionresults;

/**
 * Created by zoowii on 2015/1/17.
 */
public class HtmlResult extends PlainTextResult {

    public HtmlResult(String text) {
        super(text);
        this.contentType = "text/html; charset=" + getEncoding();
    }

    public HtmlResult(String text, String encoding) {
        super(text, encoding);
        this.contentType = "text/html; charset=" + getEncoding();
    }
}
