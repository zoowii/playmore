package com.zoowii.playmore.action.impl;

import com.zoowii.playmore.action.IRouteNotFoundExceptionHandler;
import com.zoowii.playmore.exceptions.RouteNotFoundException;
import zuice.annotations.Component;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by zoowii on 2015/1/17.
 */
@Component
public class DefaultRouteNotFoundExceptionHandler implements IRouteNotFoundExceptionHandler {
    @Override
    public void handleException(HttpServletRequest request, HttpServletResponse response, RouteNotFoundException e) {
        try {
            if (e == null || e.getMessage() == null) {
                return;
            }
            response.getWriter().write(e.getMessage());
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
