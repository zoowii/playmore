package com.zoowii.playmore.action;

import com.zoowii.playmore.action.actionresults.FileResult;
import com.zoowii.playmore.action.actionresults.HtmlResult;
import com.zoowii.playmore.action.actionresults.InputStreamResult;
import com.zoowii.playmore.action.actionresults.PlainTextResult;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;

/**
 * Created by zoowii on 2015/1/17.
 */
public class ActionResults {
    public static ActionResult ok(String text) {
        return html(text);
    }

    public static ActionResult ok(File file) {
        return file(file);
    }

    public static ActionResult ok(InputStream inputStream) {
        return stream(inputStream);
    }

    public static ActionResult ok(Object res) {
        if (res == null) {
            return null;
        }
        if (res instanceof ActionResult) {
            return (ActionResult) res;
        } else if (res instanceof String) {
            return ok((String) res);
        } else if (res instanceof File) {
            return ok((File) res);
        } else if (res instanceof InputStream) {
            return ok((InputStream) res);
        } else {
            return ok(res.toString());
        }
    }

    public static ActionResult redirect(String url) {
        return ActionResults.ok("Redirect").setStatusCode(HttpURLConnection.HTTP_MOVED_TEMP).setHeader("Location", url);
    }

    public static ActionResult text(String text) {
        return new PlainTextResult(text);
    }

    public static ActionResult text(String text, String encoding) {
        return new PlainTextResult(text, encoding);
    }

    public static ActionResult file(File file) {
        return new FileResult(file);
    }

    public static ActionResult file(String filePath, String mode) {
        return new FileResult(filePath, mode);
    }

    public static ActionResult file(String filePath) {
        return new FileResult(filePath);
    }

    public static ActionResult stream(InputStream inputStream) {
        return new InputStreamResult(inputStream);
    }

    public static ActionResult html(String htmlText) {
        return new HtmlResult(htmlText);
    }

    public static ActionResult html(String htmlText, String encoding) {
        return new HtmlResult(htmlText, encoding);
    }

    public static ActionResult serveFilesInResource(String... paths) {
        String path = StringUtils.join(paths, "/");
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
        return autoDetectContentTypeFromMimeType(stream(inputStream), paths);
    }

    public static ActionResult serveFiles(String... paths) {
        return autoDetectContentTypeFromMimeType(file(StringUtils.join(paths, File.separator)), paths);
    }

    public static String detectContentTypeFromPath(String path) {
        path = path.toLowerCase();
        if (path.endsWith(".js")) {
            return "text/javascript";
        } else if (path.endsWith(".css")) {
            return "text/css";
        } else if (path.endsWith(".html") || path.endsWith(".htm")) {
            return "text/html";
        } else if (path.endsWith(".jpg") || path.endsWith(".jpeg")) {
            return "image/jpeg";
        } else if (path.endsWith(".gif")) {
            return "image/gif";
        } else if (path.endsWith(".png")) {
            return "image/png";
        }
        return URLConnection.guessContentTypeFromName(path);
    }

    /**
     * 从文件后缀名识别出mime-type从而设置content-type
     *
     * @param actionResult
     * @param paths
     * @return
     */
    public static ActionResult autoDetectContentTypeFromMimeType(ActionResult actionResult, String... paths) {
        String path = StringUtils.join(paths, File.separator);
        actionResult.setContentType(detectContentTypeFromPath(path));
        return actionResult;
    }
}
