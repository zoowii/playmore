package com.zoowii.playmore.exceptions;

/**
 * Created by zoowii on 2015/1/17.
 */
public class CloneFailedRuntimeException extends RuntimeException {
    public CloneFailedRuntimeException() {
    }

    public CloneFailedRuntimeException(String message) {
        super(message);
    }

    public CloneFailedRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public CloneFailedRuntimeException(Throwable cause) {
        super(cause);
    }

    public CloneFailedRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
