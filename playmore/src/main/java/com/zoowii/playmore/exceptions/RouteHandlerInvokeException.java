package com.zoowii.playmore.exceptions;

/**
 * Created by zoowii on 2015/1/16.
 */
public class RouteHandlerInvokeException extends RuntimeException {
    public RouteHandlerInvokeException() {
    }

    public RouteHandlerInvokeException(String message) {
        super(message);
    }

    public RouteHandlerInvokeException(String message, Throwable cause) {
        super(message, cause);
    }

    public RouteHandlerInvokeException(Throwable cause) {
        super(cause);
    }

    public RouteHandlerInvokeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
