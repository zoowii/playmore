package com.zoowii.playmore.exceptions;

/**
 * 运行时有用的一些异常
 * Created by zoowii on 15/1/23.
 */
public class ActionUsefulRuntimeException extends RuntimeException {
    public ActionUsefulRuntimeException() {
    }

    public ActionUsefulRuntimeException(String message) {
        super(message);
    }

    public ActionUsefulRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActionUsefulRuntimeException(Throwable cause) {
        super(cause);
    }

    public ActionUsefulRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
