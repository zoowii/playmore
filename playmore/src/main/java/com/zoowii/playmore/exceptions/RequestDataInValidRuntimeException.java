package com.zoowii.playmore.exceptions;

/**
 * Created by zoowii on 15/1/22.
 */
public class RequestDataInValidRuntimeException extends RuntimeException {
    public RequestDataInValidRuntimeException() {
    }

    public RequestDataInValidRuntimeException(String message) {
        super(message);
    }

    public RequestDataInValidRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestDataInValidRuntimeException(Throwable cause) {
        super(cause);
    }

    public RequestDataInValidRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
