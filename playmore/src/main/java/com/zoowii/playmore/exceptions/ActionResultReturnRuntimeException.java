package com.zoowii.playmore.exceptions;

import com.zoowii.playmore.action.ActionResult;

/**
 * Created by zoowii on 15/1/22.
 */
public class ActionResultReturnRuntimeException extends ActionUsefulRuntimeException {
    private ActionResult actionResult;

    public ActionResultReturnRuntimeException(ActionResult actionResult) {
        this.actionResult = actionResult;
    }

    public ActionResult getActionResult() {
        return actionResult;
    }
}
