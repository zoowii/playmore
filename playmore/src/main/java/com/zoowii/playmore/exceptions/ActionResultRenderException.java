package com.zoowii.playmore.exceptions;

/**
 * Created by zoowii on 2015/1/17.
 */
public class ActionResultRenderException extends Exception {
    public ActionResultRenderException() {
    }

    public ActionResultRenderException(String message) {
        super(message);
    }

    public ActionResultRenderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActionResultRenderException(Throwable cause) {
        super(cause);
    }

    public ActionResultRenderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
