package com.zoowii.playmore.security;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.http.HttpServletRequestWrapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by zoowii on 15/1/22.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authenticated {
    public static final class DEFAULT extends Authenticator {
        @Override
        public String getUsername(HttpServletRequestWrapper request) {
            return null;
        }

        @Override
        public ActionResult onUnauthorized(HttpServletRequestWrapper request) {
            return null;
        }
    }

    public Class<? extends Authenticator> value() default DEFAULT.class;
}
