package com.zoowii.playmore.security;

import com.zoowii.playmore.action.ActionResult;
import com.zoowii.playmore.http.HttpServletRequestWrapper;

/**
 * Created by zoowii on 15/1/22.
 */
public abstract class Authenticator {
    public abstract String getUsername(HttpServletRequestWrapper request);

    public abstract ActionResult onUnauthorized(HttpServletRequestWrapper request);
}
