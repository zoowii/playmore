package com.zoowii.playmore.security;

import com.zoowii.playmore.exceptions.ActionResultReturnRuntimeException;
import com.zoowii.playmore.http.HttpUtils;
import com.zoowii.playmore.util.AnnotationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zuice.aop.JoinPoint;
import zuice.aop.annotations.After;
import zuice.aop.annotations.Aspect;
import zuice.aop.annotations.Before;

import java.lang.reflect.Method;

/**
 * Created by zoowii on 15/1/22.
 */
@Aspect
public class SecurityInterceptor {
    private static final Logger LOG = LoggerFactory.getLogger(SecurityInterceptor.class);

    @Before("securityPoint()")
    public void checkSecurity(JoinPoint joinPoint) {
        System.out.println("checking security");
        Method method = joinPoint.getOriginMethod();
        Authenticated authenticatedAnno = AnnotationUtils.getAnnotation(method, Authenticated.class);
        if (authenticatedAnno == null) {
            return;
        }
        Class<? extends Authenticator> authenticatorCls = authenticatedAnno.value();
        Authenticator authenticator;
        try {
            authenticator = authenticatorCls.newInstance();
        } catch (Exception e) {
            LOG.error("Can't create instance of authenticator", e);
            return;
        }
        String username = authenticator.getUsername(HttpUtils.getCurrentHttpRequest());
        if (username == null) {
            throw new ActionResultReturnRuntimeException(authenticator.onUnauthorized(HttpUtils.getCurrentHttpRequest()));
        }
    }

    @After("securityPoint()")
    public void after(JoinPoint joinPoint) {

    }
}
