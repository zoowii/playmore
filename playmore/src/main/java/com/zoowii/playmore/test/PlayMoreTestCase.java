package com.zoowii.playmore.test;

import com.zoowii.jpa_utils.util.StringUtil;
import com.zoowii.playmore.util.AnnotationUtils;
import junit.framework.TestCase;
import zuice.BeanFactory;
import zuice.config.xml.ClassPathXmlConfigLoader;

/**
 * Created by zoowii on 15/1/23.
 */
public class PlayMoreTestCase extends TestCase {
    protected BeanFactory injector;

    @Override
    public void setUp() {
        Class<?> testCaseCls = this.getClass();
        RunWith runWithAnno = AnnotationUtils.getAnnotation(testCaseCls, RunWith.class);
        String configPath = "playmore.xml";
        if (runWithAnno != null && !StringUtil.isEmpty(runWithAnno.value())) {
            configPath = runWithAnno.value().trim();
        }
        injector = ClassPathXmlConfigLoader.createInjector(configPath);
    }

    @Override
    public void tearDown() {
        if (injector != null) {
            injector.close();
        }
    }
}
