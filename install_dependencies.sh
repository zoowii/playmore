#!/bin/bash
mvn install:install-file -DgroupId=com.zoowii -DartifactId=zuice -Dversion=1.0.4-19 -Dpackaging=jar -Dfile=./lib/zuice-1.0.4-19.jar
mvn install:install-file -DgroupId=com.zoowii -DartifactId=jany-route -Dversion=1.0.0-beta1 -Dpackaging=jar -Dfile=./lib/jany-route-1.0.0-beta1.jar
mvn install:install-file -DgroupId=com.zoowii -DartifactId=jpa-utils -Dversion=1.9.18 -Dpackaging=jar -Dfile=./lib/jpa-utils-1.9.18.jar
mvn install:install-file -DgroupId=com.zoowii -DartifactId=formutils -Dversion=1.0.2 -Dpackaging=jar -Dfile=./lib/formutils-1.0.2.jar